import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from 'styled-bootstrap-components';
import SignInForm from './components/SignInForm';
import SignUpForm from './components/SignUpForm';
import Wall from './components/Wall';
import { ReactComponent as Spinner } from './images/spinner.svg';

const Main = props => {
	const {
		profile: { isLoaded, email },
	} = props;
	if (isLoaded && email) {
		return <Wall {...props} />;
	}
	if (isLoaded && !email) {
		return <SignInForm {...props} />;
	}
	return <Spinner style={{ margin: '30vh auto 0', display: 'block' }} />;
};

const App = otherProps => {
	return (
		<Router>
			<Container>
				<Route path="/" exact component={props => <Main {...props} {...otherProps} />} />
				<Route
					path="/sign-up"
					exact
					component={props => <SignUpForm {...props} {...otherProps} />}
				/>
			</Container>
		</Router>
	);
};

export default connect(({ firebase: { auth, profile, errors } }) => ({ auth, profile, errors }))(
	App,
);
