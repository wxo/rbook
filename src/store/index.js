import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';

// Firebase config
const firebaseConfig = {
  apiKey: 'AIzaSyCsGlj91lQWmXgeMYPUgjgABhVQ-nvl7E4',
  authDomain: 'https://rbook-b72b2.firebaseio.com',
  databaseURL: 'https://rbook-b72b2.firebaseio.com',
  // storageBucket: '<your-storage-bucket>'
};
firebase.initializeApp(firebaseConfig);

// react-redux-firebase options
const config = {
  userProfile: 'users', // firebase root where user profiles are stored
  enableLogging: false, // enable/disable Firebase's database logging
};

const createStoreWithFirebase = compose(reactReduxFirebase(firebase, config))(createStore);

const rootReducer = combineReducers({
  firebase: firebaseReducer,
});

export default createStoreWithFirebase(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
