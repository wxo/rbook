import React, { Component } from 'react';
import { firebaseConnect } from 'react-redux-firebase';
import firebase from 'firebase';
import {
	Button,
	Card,
	CardBody,
	FormGroup,
	FormText,
	FormControl,
} from 'styled-bootstrap-components';
import DropzoneWithPreview from './DropzoneWithPreview';

class PostForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			message: null,
			formMessage: null,
			disabled: false,
			hasImage: false,
			images: [],
		};

		this.baseState = this.state;
		this.textareaRef = React.createRef();
	}

	updateMessage = event => {
		this.setState({ message: event.target.value, formMessage: null });
	};

	handleImageDrop = images => {
		this.setState({
			images: images.map(image => ({
				...image,
				preview: URL.createObjectURL(image),
			})),
		});
	};

	handleFormSubmit = event => {
		event.preventDefault();

		if (this.state.message === null || this.state.message === '') {
			this.setState({ formMessage: 'Debes escribir algo.' });
			return;
		}

		this.setState({ disabled: true, formMessage: 'Enviando.' });
		firebase
			.database()
			.ref(`/posts/${this.props.auth.uid}/${new Date().getTime()}`)
			.set({ message: this.state.message })
			.then(() => {
				this.textareaRef.current.value = '';
				this.setState(this.baseState);
			});
	};

	handleImageAttachment = event => {
		event.preventDefault();
		this.setState({ hasImage: true });
	};

	render() {
		return (
			<Card>
				<CardBody>
					<form>
						<FormGroup>
							<FormControl
								innerRef={this.textareaRef}
								disabled={this.state.disabled}
								placeholder="Qué está pasando?"
								textarea
								rows="3"
								mb="10px"
								onChange={this.updateMessage}
							/>
							<FormText muted>{this.state.formMessage}</FormText>
							{this.state.hasImage ? (
								<DropzoneWithPreview
									images={this.state.images}
									handleImageDrop={this.handleImageDrop}
								/>
							) : null}
							{!this.state.hasImage ? (
								<Button primary outline onClick={this.handleImageAttachment}>
									Adjuntar Imagen
								</Button>
							) : null}
							<Button style={{ float: 'right' }} primary outline onClick={this.handleFormSubmit}>
								Enviar
							</Button>
						</FormGroup>
					</form>
				</CardBody>
			</Card>
		);
	}
}

export default firebaseConnect()(PostForm);
