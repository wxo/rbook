import React, { Component, Fragment } from 'react';
import { firebaseConnect } from 'react-redux-firebase';
import firebase from 'firebase';
import { Button, Row, Column, H1, Card, CardBody, A } from 'styled-bootstrap-components';
import PostForm from './PostForm';
import MessageList from './MessageList';

class Wall extends Component {
	logout = event => firebase.logout();

	render() {
		return (
			<Fragment>
				<Row>
					<Column sm={11}>
						<H1>Reactibook!</H1>
					</Column>
					<Column sm={1}>
						<Button mt="10px" mb="10px" ml="22px" primary outline onClick={this.logout}>
							Salir
						</Button>
					</Column>
				</Row>
				<PostForm {...this.props} />
				<MessageList {...this.props} />
				<Card mt="10px">
					<CardBody style={{ textAlign: 'center' }}>
						Hecho por <A href="mailto:juanlajara@gmail.com">Juan La Jara</A>.
					</CardBody>
				</Card>
			</Fragment>
		);
	}
}

export default firebaseConnect()(Wall);
