import React, { Component, Fragment } from 'react';
import { Button } from 'styled-bootstrap-components';

class DeletePrompt extends Component {
	state = {
		confirmed: false,
	};

	handleConfirmedButtonClicked = event => {
		const { firebase, auth, data } = this.props;

		firebase
			.database()
			.ref(`/posts/${auth.uid}/${data[0]}`)
			.remove();
	};

	handleConfirmButtonClicked = event => {
		this.setState({ confirmed: true });
	};

	handleCancelButtonClicked = event => {
		this.setState({ confirmed: false });
	};

	render() {
		return this.state.confirmed ? (
			<Fragment>
				<Button primary outline sm mr="10px" ml="10px" onClick={this.handleConfirmedButtonClicked}>
					Estás segurx?
				</Button>
				<Button primary outline sm onClick={this.handleCancelButtonClicked}>
					Cancelar
				</Button>
			</Fragment>
		) : (
			<Button primary outline sm ml="10px" onClick={this.handleConfirmButtonClicked}>
				Borrar
			</Button>
		);
	}
}

export default DeletePrompt;
