import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import styled from 'styled-components';

const dropzoneStyles = {
	borderColor: 'rgb(102, 102, 102)',
	borderRadius: '5px',
	borderStyle: 'dashed',
	borderWidth: '2px',
	height: '200px',
	position: 'relative',
	padding: '10px',
	width: 'calc(100% - 25px)',
};

const ThumbsContainer = styled.aside`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	margin-top: 16px;
`;

const Thumb = styled.div`
	border: 1px solid #eaeaea;
	border-radius: 2;
	box-sizing: border-box;
	display: inline-flex;
	height: 100%;
	margin-bottom: 8px;
	margin-right: 8px;
	padding: 4px;
	width: 100%;
`;

const ThumbInner = styled.div`
	display: flex;
	min-width: 0;
	overflow: hidden;
`;

const Image = styled.img`
	display: block;
	height: auto;
	min-width: 200px;
`;

class DropzoneWithPreview extends Component {
	componentWillUnmount() {
		const { images } = this.props;
		let iterator = images.length;
		while (iterator--) {
			const image = images[0];
			URL.revokeObjectURL(image.preview);
		}
	}

	render() {
		const { images } = this.props;

		const thumbs = images.map((image, index) => (
			<Thumb key={index}>
				<ThumbInner>
					<Image src={image.preview} />
				</ThumbInner>
			</Thumb>
		));

		return (
			<section style={{ marginTop: '20px' }}>
				<Dropzone accept="image/*" style={dropzoneStyles} onDrop={this.props.handleImageDrop}>
					Arrastre una imagen o haga click.
				</Dropzone>
				<ThumbsContainer>{thumbs}</ThumbsContainer>
			</section>
		);
	}
}

export default DropzoneWithPreview;
