import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { firebaseConnect } from 'react-redux-firebase';
import firebase from 'firebase';
import { last } from 'lodash-es';
import {
	Button,
	FormGroup,
	Label,
	FormControl,
	H1,
	Jumbotron,
	Alert,
} from 'styled-bootstrap-components';
import { ReactComponent as Spinner } from '../images/spinner.svg';

class SignInForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: null,
			password: null,
			emailInputMessage: null,
			passwordInputMessage: null,
			formMessage: null,
			loading: false,
		};

		this.baseState = this.state;
	}

	updateEmail = event => this.setState({ email: event.target.value, emailInputMessage: null });
	updatePassword = event =>
		this.setState({ password: event.target.value, passwordInputMessage: null });

	onFormSubmitted = event => {
		event.preventDefault();
		const { email, password } = this.state;

		if (email === '' || email === null) {
			this.setState({ emailInputMessage: 'Debe llenar el campo email.' });
			return;
		}

		if (password === '' || password === null) {
			this.setState({ passwordInputMessage: 'Debe llenar el campo password.' });
			return;
		}

		firebase.login({ email, password });
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		const lastError = last(nextProps.errors);
		if (lastError && lastError.code === 'auth/invalid-email') {
			return { formMessage: 'Email inválido.' };
		}
		if (lastError && lastError.code === 'auth/user-not-found') {
			return { formMessage: 'Usuario no registrado.' };
		}
		if (lastError && lastError.code === 'auth/wrong-password') {
			return { formMessage: 'Password incorrecto.' };
		}
		if (lastError === null) {
			return { loading: true };
		}
		return null;
	}

	render() {
		const { emailInputMessage, passwordInputMessage, formMessage, loading } = this.state;

		if (loading) {
			return <Spinner style={{ margin: '30vh auto 0', display: 'block' }} />;
		}

		return (
			<Jumbotron mt="20vh">
				<H1>Ingresa a Reactibook!</H1>
				<form>
					<FormGroup>
						<Label>Email</Label>
						<FormControl type="email" placeholder="Ingresa tu email" onChange={this.updateEmail} />
						{emailInputMessage ? <Alert danger>{emailInputMessage}</Alert> : null}
					</FormGroup>
					<FormGroup>
						<Label>Password</Label>
						<FormControl type="password" placeholder="Password" onChange={this.updatePassword} />
						{passwordInputMessage ? <Alert danger>{passwordInputMessage}</Alert> : null}
					</FormGroup>
					<FormGroup>
						{formMessage && <Alert danger>{formMessage}</Alert>}
						<Button onClick={this.onFormSubmitted} primary>
							Ingresar
						</Button>
					</FormGroup>
				</form>
				<Link to="/sign-up">o crea una cuenta.</Link>
			</Jumbotron>
		);
	}
}

export default firebaseConnect()(SignInForm);
