import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { firebaseConnect } from 'react-redux-firebase';
import firebase from 'firebase';
import {
	Button,
	FormGroup,
	Label,
	FormControl,
	H1,
	Jumbotron,
	Alert,
} from 'styled-bootstrap-components';

class SignUpForm extends Component {
	state = {
		email: null,
		password: null,
		emailInputMessage: null,
		passwordInputMessage: null,
	};

	updateEmail = event => this.setState({ email: event.target.value });
	updatePassword = event => this.setState({ password: event.target.value });

	onFormSubmitted = event => {
		event.preventDefault();
		const { email, password } = this.state;

		if (email === '' || email === null) {
			this.setState({ emailInputMessage: 'Debe llenar el campo email.' });
			return;
		}

		if (password === '' || password === null) {
			this.setState({ passwordInputMessage: 'Debe llenar el campo password.' });
			return;
		}

		firebase.createUser({ email, password }, { username: 'test', email });
	};

	render() {
		const { emailInputMessage, passwordInputMessage } = this.state;

		if (this.props.auth.uid) {
			this.props.history.push('/');
			return null;
		}

		return (
			<Jumbotron mt="20vh">
				<H1>Regístrate en Reactibook!</H1>
				<form>
					<FormGroup>
						<Label>Email</Label>
						<FormControl type="email" placeholder="Ingresa tu email" onChange={this.updateEmail} />
						{emailInputMessage ? <Alert danger>{emailInputMessage}</Alert> : null}
					</FormGroup>
					<FormGroup>
						<Label>Password</Label>
						<FormControl type="password" placeholder="Password" onChange={this.updatePassword} />
						{passwordInputMessage ? <Alert danger>{passwordInputMessage}</Alert> : null}
					</FormGroup>
					<FormGroup>
						<Button onClick={this.onFormSubmitted} primary>
							Regístrate
						</Button>
					</FormGroup>
				</form>
				<Link to="/">o ingresa a tu cuenta.</Link>
			</Jumbotron>
		);
	}
}

export default firebaseConnect()(SignUpForm);
