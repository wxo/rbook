import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import styled from 'styled-components';
import { es } from 'date-fns/locale';
import { formatDistance } from 'date-fns';
import { Card, CardBody, Row, Column } from 'styled-bootstrap-components';
import DeletePrompt from './DeletePrompt';

const orderPosts = posts => {
	return posts ? Object.entries(posts).reverse() : [];
};

const postOnBlur = ({ firebase, auth, data, event }) => {
	firebase
		.database()
		.ref(`/posts/${auth.uid}/${data[0]}`)
		.set({ message: event.target.textContent });
};

const FormattedTime = styled.span`
	color: gray;
	display: block;
	font-style: italic;
	font-size: 14px;
	text-align: right;
	width: 100%;
`;

const Post = ({ data, firebase, auth }) => {
	return (
		<Card mt="10px">
			<CardBody>
				<Row mb="10px">
					<Column
						onBlur={event => postOnBlur({ firebase, auth, data, event })}
						contentEditable
						suppressContentEditableWarning
						pl="10px"
						sm={9}>
						{data[1].message}
					</Column>
					<Column sm={3}>
						<FormattedTime>{formatDistance(new Date(+data[0]), new Date(), { locale: es })}</FormattedTime>
					</Column>
				</Row>
				<Row>
					<DeletePrompt data={data} firebase={firebase} auth={auth} />
				</Row>
			</CardBody>
		</Card>
	);
};

const MessageList = ({ posts, auth, firebase }) => {
	return posts
		? orderPosts(posts[auth.uid]).map(data => (
				<Post key={data[0]} auth={auth} data={data} firebase={firebase} />
		  ))
		: null;
};

export default compose(
	firebaseConnect(props => {
		return ['posts'];
	}),
	connect(state => ({ posts: state.firebase.data.posts })),
)(MessageList);
