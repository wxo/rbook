## Reactibook

Con este proyecto estoy solucionando los requerimientos mencionados en https://docs.google.com/presentation/d/1X83xY37DaTZcPR_uC_w0lfIXT_x5F-orfIj3oAAWqLw/edit#slide=id.g36b12e6439_0_121

## Reporte

### Pantalla de Sign-in

- [X] El input de usuario debe ser requerido
- [X] El input password debe ser requerido
- [X] El nombre de usuario debe ser un correo válido
- [X] Lo que tipeo en el input de password debe ser secreto
- [X] Los mensajes de error deben aparecer en la parte inferior de los inputs

- [X] Cuando presiono el botón login debe validarse el formulario en caso todo esté bien el sistema debe redireccionar a nuestro muro.
- [X] En caso la validación falle debe mostrar los campos de error y no debe redireccionar al muro.
- [] El sistema debe tener una lista de usuarios válidos y sólo se podrá ingresar con esos usuarios. Cualquier otro campo debe decir que es un usuario inválido.
- [] Al entregar el proyecto, indicar el usuario con el que se puede hacer pruebas.

Los últimos 2 puntos fueron logrados con una pantalla de Sign-up

![](https://cdn-std.dprcdn.net/files/acc_696085/2vwQhr)
![](https://cdn-std.dprcdn.net/files/acc_696085/YeUhYn)

### Pantalla de Muro

- [X] Al apretar el botón de publicar debe validar que exista texto en el input.
- [X] Debo poder publicar un post
- [X] Debo poder eliminar un post específico
- [] Debo poder filtrar los posts solo para mis amigos y para todo público
- [X] Debe confirmar antes de eliminar un post
- [X] Al darle click en el botón editar debe cambiar el texto por un input con texto y cambiar el link por guardar
- [X] Al darle guardar debe cambiar de vuelta a un texto normal pero con la información editada

Para editar el contenido de los posts hacer click en el texto, para guardar hacer click afuera o presionar la tecla tab

![](https://cdn-std.dprcdn.net/files/acc_696085/2mKSDF)
![](https://cdn-std.dprcdn.net/files/acc_696085/Lh7aZL)

Aparte de los requerimientos, he agregado:

- [X] Botón de Salir (logout)
- [X] Tiempo relativo de cada publicación ej: "hace 2 horas"

### Hacker Edition

- [X] Se debe poder crear un post con imagen
- [] Utilizar Firebase para guardar cada uno de los posts y que estos no se pierdan. Tomar en cuenta que usuarios distintos pueden tener posts distintos.

![](https://cdn-std.dprcdn.net/files/acc_696085/79vxej)
![](https://cdn-std.dprcdn.net/files/acc_696085/4FaUma)

## Observaciones

Aunque completé la funcionalidad de adjuntar imágenes a los posts, por razones de tiempo no pude completar la funcionalidad de upload a firebase. Una alternativa pudo haber sido usar Filepicker para solo enfocarme en guardar los links en Firebase.

Con respecto a la abstracción de amistades, una alternativa hubiese sido dejar la responsabilidad del filtrado de Posts a la capa de persistencia (en este caso Firebase). No tengo claro si eso hubiese sido una solución factible solo con Firebase, otras opciones sí ofrecen opciones de filtrado e intersección como RethinkDB.

Aunque el ejercicio recomienda usar Redux, aparte de usar `react-redux-firebase` para la interacción con Firebase, no tuve la necesidad de escribir algun reducer propio. `react-redux-firebase` demostró ser una herramienta muy versatil para este ejercicio y me ahorró de escribir bastante boilerplate.

Tenía en mente usar `Undux` para escribir los reducers pero como he mencionado, no me vi en la necesidad.

Para la estructura inicial de esta solución he usado `create-react-app`, usarlo me ahorró bastante tiempo de configurar Webpack y Babel. Para efectos de este ejercicio me parece que fue una buena decisión.

Para abstraer los estilos he usado Styled Components, también un library que es compatible y permite acceder a componentes de Bootstrap llamado `styled-bootstrap-components`.

Para el manejo de tiempo use `date-fns`, siendo un library que se ha hecho conocido como respuesta a `moment-js`, teniendo este un API más funcional (composable) que permite bundles más livianos al poder soportar three-shaking.

Hablando de three-shaking, también use una función de `lodash-es`, la versión compatible con módulos ES que permite hacer imports sin necesidad de preocuparse de importar todo lodash.

Para la funcionalidad de drag-and-drop use `react-dropzone`, lo cual me ahorró de escribir boilerplate para tener esa funcionalidad a la mano.

Como se puede ver en los commits, me tomo unas cuantas horas durante 2 días desarrollar esta solución, estoy contento con el resultado y tengo que decir que me divertí al escribirlo. Ver que varias herramientas pueden trabajar juntas al mismo tiempo me ha dejado con ganas de seguir investigando el ecosistema de React, especialmente ahora que React-hooks va a cambiar algunas prácticas que se estuvieron haciendo populares.
